<?php

namespace Drupal\whatsnew_dashboard;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Site entities.
 */
interface SiteInterface extends ConfigEntityInterface {

}

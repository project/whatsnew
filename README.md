# drupal_whatsnew
Drupal (8.x) module to generate an available update report
to be used alongside the What's New dashboard.
